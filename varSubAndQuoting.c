// All code below was written by Cameron S.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <stdbool.h>

char* quoteStateMachine(char* line);
int getVars(char line[], char* vars[100][2], int varNum);
bool contains(char* vars[100][2], char* varName, int varNum);
bool varSub(char line[], char* vars[100][2], int varNum);
int getFirstQuoteIndex(char* line);
int getLastQuoteIndex(char* line);
void removeFirstQuote(char* str, char c);
void removeLastQuote(char* str, char c);

char*vars[100][2];
int varNum = 0;

int main() {
	while(1) {
		char* line = readline("$ ");
		
		if (line != NULL) {
			quoteStateMachine(line);
			getVars(line, vars, varNum);
			varSub(line, vars, varNum);
			free(line);
		}
	}
}

int getVars(char line[], char* vars[100][2], int varNum) {
	for (int i = 1; i < strlen(line) - 1; i++) {
		if (!isspace(line[i - 1]) && line[i] == '=' && !isspace(line[i + 1])) {
			int varValLen = i + 1, varNameLen = i;
			while(varNameLen >= 0 && !isspace(line[varNameLen])) {
				varNameLen--;
			}
			while (varValLen < strlen(line) && (isalpha(line[varValLen]) || isdigit(line[varValLen]))) {
				varValLen++;
			}

			char* varName = strndup(line, i-varNameLen-1);
			printf("Length of varName: %d\n",i-varNameLen-1);
			if(!contains(vars, varName, varNum)) {
				char* varVal = strndup(line + i + 1, varValLen - i);
				vars[varNum][0] = varName;
				vars[varNum][1] = varVal;
				varNum++;
				printf("%s = %s\n", varName, varVal);
			}
			else {
				printf("%s is already a variable.\n", varName);
			}
		}
	}
	return varNum;
}

bool contains(char* vars[100][2], char* varName, int varNum) {
	for(int i = 0; i < varNum; i++) {
		if(strcmp(vars[i][0], varName) == 0) {
			return true;
		}
	}
	return false;
}

bool varSub(char line[], char* vars[100][2], int varNum) {
	for (int i = 0; i < strlen(line)-1; i++) {
		if (line[i] == '$' && (isalpha(line[i+1]) || isdigit(line[i+1]))) {
			int j = i + 1;
			while (isalpha(line[j]) || isdigit(line[j])) {
				j++;
			}
			char* varName = strndup(line + i + 1, j);
			for (int k = 0; k < varNum; k++) {
				printf("Current Variable: %s\n", vars[k][0]);
				if (strcmp(vars[k][0], varName) == 0) {
					char* toBeSubbed = strndup(line + i, j);
					printf("This will be subbed: %s\n", toBeSubbed);
					char* pch = strstr(line, toBeSubbed);
					strncpy(pch, vars[k][1], strlen(toBeSubbed));
					printf("line: %s\n", line);
				}
			}
		}
	}
	return true;
}

char* quoteStateMachine(char* line) {
	char* tokens[strlen(line)];
	int tokenNum = 0, firstQuoteDex = getFirstQuoteIndex(line);

	for(int i = 0; i < strlen(line); i++) {
		if(line[i] != ' ') {
			int j = i;
			if(i == firstQuoteDex) {
				while(j < strlen(line) && line[j] != line[firstQuoteDex]) {
					j++;
				}
				char* token = strndup(line+i, j-i);
				removeFirstQuote(token, line[firstQuoteDex]);
				removeLastQuote(token, line[firstQuoteDex]);
				tokens[tokenNum++] = token;
				printf("Token[%d] = %s\n", tokenNum-1, token);
				i = j;
			}
			while(j < strlen(line) && (line[j] != ' ' || (j > firstQuoteDex && firstQuoteDex != -1))) {
				j++;
			}
			char* token = strndup(line+i, j-i);
			removeFirstQuote(token, line[firstQuoteDex]);
			removeLastQuote(token, line[firstQuoteDex]);
			tokens[tokenNum++] = token;
			printf("Token[%d] = %s\n", tokenNum-1,  token);
			i = j;
		}
	}
}

int getFirstQuoteIndex(char* line) {
	for (int i = 0; i < strlen(line); i++) {
		if (line[i] == '\'' || line[i] == '"' || line[i] == '`') {
			return i;
		}
	}
	return -1;
}

int getLastQuoteIndex(char* line) {
	for(int i = strlen(line)-1; i >= 0; i--) {
		if(line[i] == '\'' || line[i] == '"' || line[i] == '`') {
			return i;
		}
	}
	return -1;
}

void removeFirstQuote(char* str, char c) {
	int temp = 1;
	for(int i = 0; i < strlen(str); i++) {
		if(temp) {
			if(c == str[i]) {
				temp = 0;
				str[i] = str[i+1];
			}
		}
		else {
			str[i] = str[i+1];
		}
	}
}

void removeLastQuote(char* str, char c) {
	int i, index = -1;
	for(int i = 0; i < strlen(str); i++) {
		if(str[i] == c) {
			index = i;
		}
	}
	if(index != -1) {
		i = index;
		while(i < strlen(str)) {
			str[i] = str[i+1];
			i++;
		}
	}
}
