#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <limits.h>
#include <sys/wait.h>

// All code in this file written by Merddyn Sheeley

#define READ 0
#define WRITE 1
#define DEBUGMODE 0

int getPipeCount(char*);


int main()
{
	// Split off this section of code into a new function:
	char* homeaddress;
	homeaddress = getenv("HOME");

	char* configFilePath;
	configFilePath = malloc(strlen(homeaddress) + 7);
	strcpy(configFilePath, homeaddress);
	strcat(configFilePath, "/.xshrc");
#if DEBUGMODE
	printf("home: %s\n", homeaddress);
	printf("path: %s\n", configFilePath);
#endif
	// End section.

	int configFD = open(configFilePath, O_RDONLY);

#if DEBUGMODE
	printf("Config file descriptor: %d\n", configFD);
#endif

	free(configFilePath);
	int pipeCount = 0;
	int wstatus;
	int** pipes; // a 2-dimensional array
	char* line;
	char* vars[100][2];
	int varNum = 0;
	char* token;
	char** stage;
	
	while (true)
	{
		line = readline("Enter a string: ");

		if (line != NULL)
		{
			if (!strcmp(line, "exit"))
				exit(0);
			pipeCount = getPipeCount(line);
#if DEBUGMODE
			printf("There are %d pipes", pipeCount);
			printf("\n");
#endif
			if (pipeCount > 0)
			{
				pipes = (int**)malloc(pipeCount * sizeof(int*));
				for (int i = 0; i < pipeCount; i++)
				{
					pipes[i] = (int*)malloc(2 * sizeof(int));
					if (pipe(pipes[i]) < 0)
					{
						perror("pipe creation error");
					}
				}
			}
			token = strtok(line, "|");
			stage = (char**)malloc((pipeCount + 1) * sizeof(char*));
			stage[0] = token;
			for (int i = 0; i < pipeCount; i++)
			{
				token = strtok(NULL, "|");

				if (token == NULL)
				{
					printf("Exiting loop from if\n");
					break;
				}
				stage[i + 1] = token;
			}

#if DEBUGMODE
			printf("Reprinting commands!!!\n");

			for (int i = 0; i < (pipeCount + 1); i++)
			{
				printf("String number %d: %s\n", i, stage[i]);
			}
			printf("\n");
			printf("\n");
#endif	
			int PID = fork();
			
			if (PID == 0) // Is child process of the shell.
			{
				if (pipeCount > 0)
				{					
					for(int i = 0; i < pipeCount; i++)
					{
						if(pipe(pipes[i]) < 0)
						{
							perror("Pipe creation error\n");
							exit(-1);
						}
					}
					int returnVal;
					for(int i = 0; i < pipeCount; i++)
					{
#if DEBUGMODE
						printf("Attempting loop pipeline!\n");
#endif
						PID = fork();
						if ((i == (pipeCount - 1)) && (PID == 0)) // Base case - no more processes to run.
						{	
							close(pipes[i][READ]);
							for (int j = 0; j < pipeCount; j++) // Close all but final pipe end.
							{
								if (j == i)
								{
									break;
								}
								else
								{
									close(pipes[j][READ]);
									close(pipes[j][WRITE]);
								}
							}

							if (dup2(pipes[i][WRITE], STDOUT_FILENO) < 0)
							{
								perror("childmost dup2\n");
								exit(-2);
							}
							returnVal = execlp("ls", "ls", NULL);
							if (returnVal != 0)
							{
								printf("ls failed. \n");
								exit(-1);
							}
						}

						if (PID != 0) // Current "parent" process
						{
							if (i == 0)
							{
								close(pipes[0][WRITE]);
								for (int j = 1; j < pipeCount; j++)
								{
										close(pipes[j][READ]);
										close(pipes[j][WRITE]);
								}
							}
							if (i != 0)
							{
								for (int j = 0; j < pipeCount; j++)
								{
									if (j != i-1)
									{
										close(pipes[j][WRITE]);
									}
									if (j != (i))
									{
										close(pipes[j][READ]);
									}
								}
							}
							int formeri = i; // just in case race condition overwrite.
#if DEBUGMODE
							printf("Waiting for child of %d\n", formeri);
#endif
							waitpid(PID, &wstatus, 0);

#if DEBUGMODE
							printf("Child of %d finished.\n", formeri);
#endif
							if (i != 0)
							{
								
								if (dup2(pipes[i-1][WRITE], STDOUT_FILENO) < 0)
								{
									perror("child  write  dup2\n");
									exit(-2);
								}
								if(dup2(pipes[i][READ], STDIN_FILENO) < 0)
								{
									perror("child  read dup2\n");
									exit(-2);
								}
							}
							else if (i == 0)
							{

								if (dup2(pipes[0][READ], STDIN_FILENO) < 0)
									
								{
									perror("parentmost write  dup2\n");
									exit(-2);
								}
							}
							if (((i % 2) && (pipeCount % 2)))
							{
								
#if DEBUGMODE
								perror("To uppercase ");
#endif
								returnVal = execl("/usr/bin/tr", "tr", "[:lower:]", "[:upper:]", NULL);
							}
							else if ((!(i % 2) && (pipeCount % 2)))
							{
#if DEBUGMODE
								perror("To uppercase");
#endif								
								returnVal = execl("/usr/bin/tr", "tr", "[:lower:]", "[:upper:]", NULL);
							}
							else
							{
#if DEBUGMODE
								perror("To lowercase ");
#endif
								returnVal = execl("/usr/bin/tr", "tr", "[:upper:]", "[:lower:]", NULL);
							}

							if (returnVal != 0)
							{
								perror("tr failed: command not found.\n");
								exit(-1);
							}
						}					
					}
	
				}
				else // no pipes
				{
					printf("This is where I'd put my single exec statment... IF I HAD ONE!\n");
					exit(0);
				}
		
			}
			else // Is original parent process.
			{
				printf("Waiting for child\n");
				waitpid(PID, &wstatus, 0);
				printf("Child finished\n");
			}
			free(line);
			if (pipeCount > 0)
				free(pipes);
			free(stage);
		}
	}
	close(configFD);
}



int getPipeCount(char* commandLine)
{
	int count = 0;
	for (int i = 0; commandLine[i] != '\0'; i++)
	{
		if ('|' == commandLine[i])
		{
			count++;
		}
	}
	return count;
}

