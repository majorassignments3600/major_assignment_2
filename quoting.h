#ifndef QUOTING_H
#define QUOTING_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <stdbool.h>
#include "Vars.h"

void varSub(char *line, VarsArray* list);
char *handleQuotes(char *line);

#endif