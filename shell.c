#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <unistd.h>
#include <dirent.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <fcntl.h>
#include "quoting.h"
#include "Vars.h"

int main() {

    char* prompt = calloc(1024,sizeof(char));
    VarsArray list;
    list.len = 0;
    list.variable = NULL;

    char* homeaddress;
    char* configFilePath;
    char* line;
    char fileline[2048];
	homeaddress = getenv("HOME");
	configFilePath = calloc(strlen(homeaddress) + 6, sizeof(char));
	strcpy(configFilePath, homeaddress);
	strcat(configFilePath, ".xshrc");
	int configFD = open(configFilePath, O_RDONLY);

	while(1){

        if(0){
            //Read from file
            fgets(fileline,2048,configFD);
            line = fileline;
        }else{
            //Read from user
            sprintf(prompt,"%s : %s$ ",getenv("USER"),getenv("PWD"));
		    line = readline(prompt);
        }
        

        //Save variable if any
        varSub(line,&list);
        
        //Handle quotes
        line = handleQuotes(line);

        //printf("%s\n",line);
        add_history(line);

        if(!strcmp(line,"exit")){
            free(line);
            break;
        }
        int proc = fork();
        if(proc==0){
            execl(line, NULL);
        }
        
        if(!proc){
            while(1){

            }
        }
            
        free(line);
	}

    free(prompt);
    deleteVars(&list);
	return 0;
}