#ifndef VARS_H
#define VARS_H

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct{
    char* var;
    char* value;
}Vars;

typedef struct{
    int len;
    Vars* variable;
}VarsArray;

void saveVar(VarsArray* list, char* var, char* value);
void deleteVars(VarsArray* list);

#endif