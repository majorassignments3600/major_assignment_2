#include"Vars.h"

void saveVar(VarsArray* list, char* var, char* value){

    if(!list->len)
        list->variable = calloc(1,sizeof(Vars));
    else
        list->variable = realloc(list->variable, (list->len+1)*sizeof(Vars));

    list->variable[list->len].var = calloc(strlen(var)+1,sizeof(char));
    list->variable[list->len].value = calloc(strlen(value)+1,sizeof(char));

    strcpy(list->variable[list->len].var,var);
    strcpy(list->variable[list->len].value,value);

    list->len++;
}

void deleteVars(VarsArray* list){
    int i;
    if(!list->len)
        return;
    for(i=0; i<list->len; i++){
        free(list->variable[i].value);
        free(list->variable[i].var);
    }
    free(list->variable);

    list->len = 0;
    list->variable = NULL;
}